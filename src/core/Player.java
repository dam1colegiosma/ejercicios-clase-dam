package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Player {

	String name;
	
	float hungry;
	
	float life;
	
	float money;
	
	public Player(String n) {
		this.name = n;
		this.hungry = 5;
		this.money = 20;
		this.life = 100;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getHungry() {
		return hungry;
	}

	public void setHungry(float hungry) {
		this.hungry = hungry;
	}
	
	
	public Player getPlayer()
	{
		return this;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nombre: "+this.name+"\n"+"Hambre: "+this.hungry+"/100\n"+"\n"+"Vida: "+this.life+"/100\n"+"Dinero: "+this.money;
	}

	public float getMoney() {
		return money;
	}

	public void setMoney(float money) {
		this.money = money;
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}
	
	
	public void save()
	{
		FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("C:/Users/Profesor/profile_"+this.getName()+".txt");
            pw = new PrintWriter(fichero);

            
                pw.println(this.getName());
                pw.println(this.getLife());
                pw.println(this.getHungry());
                pw.println(this.getMoney());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}
	
	public void load()
	{
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	         archivo = new File ("C:/Users/Profesor/profile_"+this.getName()+".txt");
	         if (archivo.exists()) { 
	        	 fr = new FileReader (archivo);
		         br = new BufferedReader(fr);

		         // Lectura del fichero
		         String linea;
		        
		        
		         linea=br.readLine();
		        	 
		        	 this.setName(linea);
		        	 linea=br.readLine();
		        	 this.setLife(Float.valueOf(linea));
		        	 linea=br.readLine();
			         this.setHungry(Float.valueOf(linea));
			         linea=br.readLine();
			         this.setMoney(Float.valueOf(linea));
		        
	         }else {
	        	 System.out.println("no existe");
	         }
	         
	            
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
	}

	public void sleep() {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
}
